docker build -t surfiend/multi-client-k8s:latest -t surfiend/multi-client-k8s:$SHA -f ./client/Dockerfile ./client
docker build -t surfiend/multi-server-k8s-pgfix:latest -t surfiend/multi-server-k8s-pgfix:$SHA -f ./server/Dockerfile ./server
docker build -t surfiend/multi-worker-k8s:latest -t surfiend/multi-worker-k8s:$SHA -f ./worker/Dockerfile ./worker

docker push surfiend/multi-client-k8s:latest
docker push surfiend/multi-server-k8s-pgfix:latest
docker push surfiend/multi-worker-k8s:latest

docker push surfiend/multi-client-k8s:$SHA
docker push surfiend/multi-server-k8s-pgfix:$SHA
docker push surfiend/multi-worker-k8s:$SHA

kubectl apply -f k8s
kubectl set image deployments/server-deployment server=surfiend/multi-server-k8s-pgfix:$SHA
kubectl set image deployments/client-deployment client=surfiend/multi-client-k8s:$SHA
kubectl set image deployments/worker-deployment worker=surfiend/multi-worker-k8s:$SHA